<?php include_once('header.php'); ?>
<div class="col-md-12">
    <h1>Welcome <?php echo $_SESSION['userdata']['fname']; ?> <?php echo $_SESSION['userdata']['lname']; ?></h1>
    <hr />
    <?php 
        if($_SESSION['userdata']['usertype'] == 'admin'){
            $qry1 = "SELECT id FROM users";
            $res1 = $connect->query($qry1);
            $count = $res1->num_rows;

            $qry2 = "SELECT id FROM parkings";
            $res2 = $connect->query($qry2);
            $countparkings = $res2->num_rows;

            $qry3 = "SELECT amount FROM booking";
            $res3 = $connect->query($qry3);
            $countbooking = $res3->num_rows;
            
            
            $totalearn = 0;
            while($arr = $res3->fetch_assoc()){
                $totalearn = $totalearn + $arr['amount'];
            }

            ?>
        <div class="row">
            <div class="col-md-3">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner p-2">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <h2><?php echo $count; ?></h2>
                                <span>Members Registered</span>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class='col-md-3'>
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner p-2">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <h2><?php echo $countparkings; ?></h2>
                                <span>Total Parkings</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="overview-item overview-item--c3">
                    <div class="overview__inner p-3">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-calendar-note"></i>
                            </div>
                            <div class="text">
                                <h2><?php echo $countbooking; ?></h2>
                                <span>Total Bookings Till Now</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class='col-md-3'>
                <div class="overview-item overview-item--c4">
                    <div class="overview__inner p-3">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-money"></i>
                            </div>
                            <div class="text">
                                <h2><?php echo "Rs. " . $totalearn; ?></h2>
                                <span>Total Earning From Bookings</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- <h2>Recent Bookings</h2>
                <hr /> -->
            </div>
        </div>
         
            <?php
        }
    ?>
    
</div>
<?php include_once('footer.php'); ?>