<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="vendor/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="vendor/qrcode.js"></script>
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all" />
</head>
<body>
    <div id="qrcode" style="width:300px; height:300px; margin-top:15px;" class="mx-auto"></div>
    <script>
        let qrcode = new QRCode(document.getElementById("qrcode"), {
            width : 300,
            height : 300
        });
        function makeCode () {		
            var elText = '<?php echo $_GET['id'] ?>';
            qrcode.makeCode(elText);
        }
        makeCode();
    </script>
    <div class="text-center">
        <div><?php echo $_GET['id']; ?></div>
        <a href="showbookings.php" class="btn btn-success mt-5">Back To Booking Page</a>
    </div>
</body>
</html>