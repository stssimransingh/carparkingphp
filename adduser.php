<?php include_once('header.php'); ?>
<div class="col-md-12 bg-light p-5">
    <h1 class="mb-3">Add user</h1>
    <hr/>
    <div class="login-form">
        <?php 
            if(isset($_POST['fname'])){
                $fname = $_POST['fname'];
                $lname = $_POST['lname'];
                $username = $_POST['username'];
                $email = $_POST['email'];
                $contact = $_POST['contact'];
                $password = md5($_POST['password']);
                $usertype = $_POST['usertype'];
                $qry = "INSERT INTO users (fname, lname, username, email, contact, password, usertype) 
                VALUES ('$fname','$lname','$username','$email','$contact','$password','$usertype')";
                if($connect->query($qry)){
                    echo "Register Successfully";
                }else{
                    echo "OOPS there is some error".$connect->error;
                }
            }
        ?>
        <form action="" method="post">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>First Name</label>
                        <input class="au-input au-input--full" type="text" name="fname" placeholder="First Name" required/>
                    </div>        
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Last Name</label>
                        <input class="au-input au-input--full" type="text" name="lname" placeholder="Last Name" />
                    </div>        
                </div>
            </div>
            <div class="form-group">
                <label>Username</label>
                <input class="au-input au-input--full" type="text" name="username" placeholder="Username" required/>
            </div>
            <div class="form-group">
                <label>Email Address</label>
                <input class="au-input au-input--full" type="email" name="email" placeholder="Email" required />
            </div>
            <div class="form-group">
                <label>Contact Number</label>
                <input class="au-input au-input--full" type="number" name="contact" placeholder="Contact Number" required />
            </div>
            <div class="form-group">
                <label>Password</label>
                <input class="au-input au-input--full" type="password" name="password" placeholder="Password" required />
            </div>
            <div class="form-group">
                <label>User Type</label>
                <select name="usertype" id="" class="au-input au-input--full">
                    <option value="user">User</option>
                    <option value="admin">Admin</option>
                    <option value="checker">Checker</option>
                </select>
            </div>    
            <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">register</button>
            
        </form>
    </div>
</div>
<?php include_once('footer.php'); ?>