-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2019 at 05:15 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parking`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `intime` varchar(256) NOT NULL,
  `outtime` varchar(256) NOT NULL,
  `parking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehicle_number` varchar(200) NOT NULL,
  `paymentmode` varchar(200) DEFAULT NULL,
  `amount` varchar(100) NOT NULL,
  `paymentstatus` varchar(200) DEFAULT NULL,
  `parkingstatus` int(11) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `intime`, `outtime`, `parking_id`, `user_id`, `vehicle_number`, `paymentmode`, `amount`, `paymentstatus`, `parkingstatus`, `timestamp`) VALUES
(1, '1555606800', '1555610400', 1, 2, 'PBX3', 'paytm', '20', 'success', 1, '2019-04-18 03:58:58'),
(2, '1555576140', '1555579800', 1, 2, 'PBX4', 'paytm', '20', 'success', 0, '2019-04-18 04:01:47'),
(3, '1555610400', '1555614000', 1, 2, 'PBX5', 'paytm', '20', 'success', 0, '2019-04-18 04:13:59'),
(6, '1555606800', '1555610400', 1, 2, 'PBX7', 'paytm', '20', 'success', 0, '2019-04-18 13:28:36'),
(7, '1555655400', '1555662600', 1, 2, 'PBX10', 'paytm', '40', 'success', 0, '2019-04-19 05:06:43'),
(8, '1555723740', '1555957740', 1, 6, 'PBAC', 'paytm', '1300', 'success', 1, '2019-04-19 11:52:45');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `message` varchar(500) NOT NULL,
  `status` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `message`, `status`, `timestamp`) VALUES
(1, 'Simran Singh', 'admin@simransingh.in', '9781333368', '', 'Offer', '2019-04-19 05:19:39');

-- --------------------------------------------------------

--
-- Table structure for table `parkings`
--

CREATE TABLE `parkings` (
  `id` int(11) NOT NULL,
  `parking_name` varchar(250) NOT NULL,
  `parking_address` text NOT NULL,
  `parking_space` int(11) NOT NULL,
  `parking_price` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parkings`
--

INSERT INTO `parkings` (`id`, `parking_name`, `parking_address`, `parking_space`, `parking_price`, `timestamp`) VALUES
(1, 'VR Punjab', 'NH-95, Kharar - CHD Road, Mohali', 400, '20', '2019-04-17 08:08:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `password` varchar(250) NOT NULL,
  `usertype` varchar(100) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `username`, `email`, `contact`, `password`, `usertype`, `timestamp`) VALUES
(1, 'Simran', 'Singh', 'simran', 'admin@simransingh.in', '9781333368', '55495F4063D073295E3E3882810ED62C', 'admin', '2019-04-17 06:56:23'),
(2, 'The', 'User', 'theuser', 'user@gmail.com', '9898989898', '55495f4063d073295e3e3882810ed62c', 'user', '2019-04-17 17:43:31'),
(3, 'Checker', 'Boy', 'checker', 'checker@gmail.com', '9787987878', '55495f4063d073295e3e3882810ed62c', 'checker', '2019-04-18 05:03:59'),
(5, '1`1', '1`1', '1`1`1`', '1`1@1.com', '1111111111', '403536f6614f2987174abc2e0f265956', 'user', '2019-04-19 11:19:23'),
(6, 'qqq', 'qqq', 'qqqqqq', 'q@q.com', '9780131743', '14d4710ee101cb8b81f99b2d050c1f18', 'user', '2019-04-19 11:50:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parkings`
--
ALTER TABLE `parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `parkings`
--
ALTER TABLE `parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
