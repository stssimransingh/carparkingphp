<?php include_once('header.php'); ?>
<div class="col-md-12 bg-light p-5">
    <h1 class="mb-3">All Users</h1>
    <hr/>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $qry = "SELECT * FROM users";
                $result = $connect->query($qry);
                $i = 1;
                while($row = $result->fetch_assoc()){
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td>$row[fname]</td>";
                    echo "<td>$row[lname]</td>";
                    echo "<td>$row[email]</td>";
                    $userid = $row['id'];
                    echo "<td><a href='delete.php?user_id=$userid' class='btn btn-danger'>Delete</a></td>";
                    echo "</tr>";
                    $i++;
                }
            ?>
        </tbody>
    </table>
</div>
<?php include_once('footer.php'); ?>