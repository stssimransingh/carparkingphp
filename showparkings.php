<?php include_once('header.php'); ?>
<div class="col-md-12 bg-light p-5">
    <h1 class="mb-3">Show Parkings</h1>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>Parking Name</th>
                <th>Parking Address</th>
                <th>Parking Space</th>
                <th>Parking Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $qry = "SELECT * FROM parkings";
                $result = $connect->query($qry);
                $i = 1;
                while($row = $result->fetch_assoc()){
                    echo "<tr>";
                    echo "<td>$i</td>";
                    echo "<td>$row[parking_name]</td>";
                    echo "<td>$row[parking_address]</td>";
                    echo "<td>$row[parking_space]</td>";
                    echo "<td>$row[parking_price]</td>";
                    $parking_id = $row['id'];
                    echo "<td><a href='delete.php?parking_id=$parking_id' class='btn btn-danger'>Delete</a></td>";
                    echo "</tr>";
                    $i++;
                }
            ?>
        </tbody>
    </table>
</div>
<?php include_once('footer.php'); ?>