<?php include_once('header.php'); ?>
<div class="col-md-12 bg-light p-5">
    <h1 class="mb-3">Check Qr Code</h1>
    <form action="" method="post">
        <input type="text" name="booking_id" class="form-control" placeholder="Booking Id"/>
        <input type="submit" class="btn btn-success mt-3" />
    </form>
    <?php
        if(isset($_POST['booking_id'])){
            $bookingqry = $_POST['booking_id'];
            $qry = "SELECT parkings.parking_name AS pname ,parkings.parking_price as parking_price, booking.intime AS intime ,booking.id AS bookingid, booking.outtime AS outtime,booking.user_id AS user_id,  booking.paymentstatus AS paymentstatus , booking.amount AS amount, booking.vehicle_number AS vehicle_number, booking.parkingstatus AS parkingstatus FROM booking INNER JOIN parkings ON parkings.id = booking.parking_id WHERE booking.id = '$bookingqry'";
            $result = $connect->query($qry);
            $data = $result->fetch_assoc();
            // print_r($data);
            if($data['paymentstatus']=='success'){
                $status = "Confirmed";
                $color = "text-success";
            }else{
                $status = "Not Confirmed";
                $color = "text-danger";
            }
            if($result->num_rows > 0){
            ?>
                <table class="table table-bordered table-striped mt-5">
                    <tbody>
                        <tr>
                            <th>Parking Name</th>
                            <td><?php echo $data['pname']; ?></td>
                        </tr>
                        <tr>
                            <th>Parking Status</th>
                            <td class="<?php echo $color; ?>"><?php echo $status; ?></td>
                        </tr>
                        <tr>
                            <th>In Time</th>
                            <td><?php echo date('d-m-Y H:i:s', $data['intime']); ?></td>
                        </tr>
                        <tr>
                            <th>Out Time</th>
                            <td><?php echo date('d-m-Y H:i:s', $data['outtime']); ?></td>
                        </tr>
                        <tr>
                            <th>Amount</th>
                            <td><?php echo $data['amount']; ?></td>
                        </tr>
                    </tbody>
                </table>
                
            <?php
                $user_id = $data['user_id'];
                $bookingid = $data['bookingid'];
                if($data['parkingstatus'] == 0){
                    echo "<a href='changestatus.php?user_id=$user_id&bookingid=$bookingid&status=1' class='btn btn-success mt-3'>User In</a>";
                }else if($data['parkingstatus'] == 1){
                    echo "<a href='changestatus.php?user_id=$user_id&bookingid=$bookingid&status=2' class='btn btn-info mt-3'>User Out</a>";
                }else if($data['parkingstatus'] == 2){
                    echo "Coupon Expired";
                }
            }else{
                echo "Id Not Found";
            }
        }
    ?>
</div>
<?php include_once('footer.php'); ?>