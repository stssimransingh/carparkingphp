<?php 
include_once('logincheck.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard</title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="dashboard.php">
                            <!-- <img src="images/icon/logo.png" alt="CoolAdmin" /> -->
                            <h1><?php echo SITETITLE; ?></h1>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                    <li class="has-sub">
                        <a class="js-arrow" href="dashboard.php">
                            <i class="fas fa-tachometer-alt"></i>Dashboard
                        </a>
                    </li>
                    <?php 
                        if($_SESSION['userdata']['usertype'] == 'admin'){
                    ?>
                      
                        <li class="has-sub">
                            <a class="js-arrow" href="addparking.php">
                                <i class="fas fa-plus-square"></i>Add Parking
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="showparkings.php">
                                <i class="fas fa-table"></i>Show Parking
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="adduser.php">
                                <i class="fas fa-user"></i>Add User
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="allusers.php">
                                <i class="fas fa-users"></i>All User
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="contacts.php">
                                <i class="fas fa-address-book"></i>Contacts
                            </a>
                        </li>
                    <?php   }else if($_SESSION['userdata']['usertype'] == 'user'){
                        ?>
                            <li class="has-sub">
                            <a class="js-arrow" href="bookparking.php">
                                <i class="fas fa-tachometer-alt"></i>Book Parking
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="showbookings.php">
                                <i class="fas fa-tachometer-alt"></i>Show Bookings
                            </a>
                        </li>
                    <?php
                        }else if($_SESSION['userdata']['usertype'] == 'checker'){
                            ?>
                        <li class="has-sub">
                            <a class="js-arrow" href="checkqrcode.php">
                                <i class="fas fa-tachometer-alt"></i>Check QR Code
                            </a>
                        </li>
                        
                            <?php
                        }
                    ?>
                        <li class="has-sub">
                            <a class="js-arrow" href="logout.php">
                                <i class="fas fa-sign-out-alt"></i>Log Out
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="dashboard.php">
                    <!-- <img src="images/icon/logo.png" alt="Cool Admin" /> -->
                    <h2><?php echo SITETITLE; ?></h2>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                    <li class="has-sub">
                        <a class="js-arrow" href="dashboard.php">
                            <i class="fas fa-tachometer-alt"></i>Dashboard
                        </a>
                    </li>
                    <?php 
                        if($_SESSION['userdata']['usertype'] == 'admin'){
                    ?>
                      
                        <li class="has-sub">
                            <a class="js-arrow" href="addparking.php">
                                <i class="fas fa-plus-square"></i>Add Parking
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="showparkings.php">
                                <i class="fas fa-table"></i>Show Parking
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="adduser.php">
                                <i class="fas fa-user"></i>Add User
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="allusers.php">
                                <i class="fas fa-users"></i>All User
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="contacts.php">
                                <i class="fas fa-address-book"></i>Contacts
                            </a>
                        </li>
                    <?php   }else if($_SESSION['userdata']['usertype'] == 'user'){
                        ?>
                            <li class="has-sub">
                            <a class="js-arrow" href="bookparking.php">
                                <i class="fas fa-tachometer-alt"></i>Book Parking
                            </a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="showbookings.php">
                                <i class="fas fa-tachometer-alt"></i>Show Bookings
                            </a>
                        </li>
                    <?php
                        }else if($_SESSION['userdata']['usertype'] == 'checker'){
                            ?>
                        <li class="has-sub">
                            <a class="js-arrow" href="checkqrcode.php">
                                <i class="fas fa-tachometer-alt"></i>Check QR Code
                            </a>
                        </li>
                        
                            <?php
                        }
                    ?>
                        <li class="has-sub">
                            <a class="js-arrow" href="logout.php">
                                <i class="fas fa-sign-out-alt"></i>Log Out
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="header-wrap">
                            <form class="form-header" action="" method="POST">
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                </div>
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                            <!-- <img src="images/icon/avatar-01.jpg" alt="John Doe" /> -->
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#"><?php 
                                                echo $_SESSION['userdata']['fname'];
                                                echo " ";
                                                echo $_SESSION['userdata']['lname'];
                                            ?></a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="">
                                                        <!-- <img src="images/icon/avatar-01.jpg" alt="John Doe" /> -->
                                                    </a>
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#"><?php 
                                                            echo $_SESSION['userdata']['fname'];
                                                            echo " ";
                                                            echo $_SESSION['userdata']['lname'];
                                                        ?></a>
                                                    </h5>
                                                    <span class="email">
                                                        <?php echo $_SESSION['userdata']['email']; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="account-dropdown__footer">
                                                <a href="logout.php">
                                                    <i class="zmdi zmdi-power"></i>Logout
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">