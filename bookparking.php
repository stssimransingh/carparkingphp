<?php include_once('header.php'); ?>
<div class="col-md-12 p-5 bg-light">
    <h1 class="mb-3">Book Parking</h1>
    <div class="row">
        <div class="col-md-6">
            <?php 
                if(isset($_POST['intime'])){
                    $currenttime = time();
                    $intime = strtotime($_POST['intime']);
                    $outtime = strtotime($_POST['outtime']);
                    $parking_id = $_POST['parking_id'];
                    $vehicle_number = $_POST['vehicle_number'];
                    $user_id = $_SESSION['userdata']['id'];
                    $pricepark = "SELECT * FROM parkings WHERE id='$parking_id'";
                    $res = $connect->query($pricepark);
                    $pardata = $res->fetch_assoc();
                    //get difference
                    $seconds_diff =  $outtime - $intime;
                    $time = ($seconds_diff/3600);
                    $parkprice = round($time * $pardata['parking_price']);
                    if($outtime < $intime){
                        echo "<div class='alert alert-danger'>Out Time Shoul be Greter Than In Time</div>";
                    }else if($currenttime > $intime){
                        echo "<div class='alert alert-danger'>Sorry Parking Not available for this time</div>";
                    }else{
                        $qry = "INSERT INTO booking (intime, outtime, parking_id, user_id, vehicle_number, amount)
                        VALUES('$intime','$outtime','$parking_id','$user_id','$vehicle_number','$parkprice')";
                        if($connect->query($qry)){
                            $insid = $connect->insert_id;
                            echo "<div class='alert alert-success'>Booking Successfull Please Pay Rs: $parkprice to confirm your booking. <a href='payment.php?booking_id=$insid&amount=$parkprice'>Pay Now</a></div>";
                        }else{
                            echo "<div class='alert alert-danger'>Error While Booking</div>";
                        }
                    }
                    
                }
                $today = date('Y-m-d');
                $lastdate = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 5, date('Y')));

            ?>
            <form action="" method="post">
            <label for="">In Date and Time</label>
            <input type="datetime-local" min="<?php echo $today; ?>T00:00" max="<?php echo $lastdate; ?>T00:00" class="form-control" name="intime" required />
            <label for="">Out Date and Time</label>
            <input type="datetime-local" min="<?php echo $today; ?>T00:00" max="<?php echo $lastdate; ?>T00:00" class="form-control" name="outtime" required />
            <label for="">Select Parking</label>
            <select class="form-control" name="parking_id">
                <?php 
                    $qry = "SELECT * FROM parkings";
                    $res = $connect->query($qry);
                    function getparkingcount($pid){
                        global $connect;
                        $qry1 = "SELECT * FROM booking WHERE parking_id = '$pid'";
                        $result = $connect->query($qry1);
                        $i = 0;
                        if($result->num_rows > 0){

                            while($row = $result->fetch_assoc()){
                                if($row['outtime'] < time()){

                                }else if($row['parkingstatus'] == 2){

                                }else{
                                    $i++;
                                }
                            }
                        }
                        return $i;
                    }
                    while($row = $res->fetch_assoc()){
                        $reserved = getparkingcount($row['id']);
                        $totalavailabel = $row['parking_space'] - $reserved;
                        echo "<option value='".$row['id']."'>".$row['parking_name']." (".$totalavailabel." Available)</option>";
                    }
                ?>
            </select>
            <label for="">Vehicle Number</label>
            <input type="text" class="form-control" name="vehicle_number" required />
            <input type="submit" class="btn btn-success mt-3" />
            </form>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>