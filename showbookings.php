<?php include_once('header.php'); ?>
<div class="col-md-12 p-5 bg-light">
    <h1 class="mb-3">Show Bookings</h1>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>Parking Name</th>
                <th>In DateTime</th>
                <th>Out DateTime</th>
                <th>Vehicle Number</th>
                <th>Total Amount</th>
                <th>Parking Status</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 1;
                $currenttime = time();
                $user_id = $_SESSION['userdata']['id'];
                $qry = "SELECT parkings.parking_name AS pname ,booking.parkingstatus AS parkingstatus, parkings.parking_price as parking_price, booking.intime AS intime ,booking.id AS bookingid, booking.outtime AS outtime,booking.paymentstatus AS paymentstatus , booking.amount AS amount, booking.vehicle_number AS vehicle_number FROM booking INNER JOIN parkings ON parkings.id = booking.parking_id WHERE booking.user_id = '$user_id'";
                $result = $connect->query($qry);
                // echo $result->num_rows;
                while($row = $result->fetch_assoc()){
                    echo "<tr>";
                    echo "<td>$i</td>";
                    $i++;
                    echo "<td>".$row['pname']."</td>";
                    
                    echo "<td>".date('d-m-Y -- H:i',$row['intime'])."</td>";
                    echo "<td>".date('d-m-Y -- H:i',$row['outtime'])."</td>";
                    echo "<td>".$row['vehicle_number']."</td>";
                    $seconds_diff =  $row['outtime'] - $row['intime'];
                    $time = ($seconds_diff/3600);
                    echo "<td>".round($time * $row['parking_price'])."</td>";
                    $bookid = $row['bookingid'];
                    $amount = $row['amount'];
                    if($row['paymentstatus']=='success'){
                        if($currenttime > $row['outtime']){
                            $seat = "Expired";
                        }else if($row['parkingstatus']==0){
                            $seat = "Confirmed
                                <a href='showbarcode.php?id=$bookid' class='btn btn-success btn-sm'>Show Barcode</a>
                            ";
                        }else if($row['parkingstatus']==1){
                            $seat = "Car Parked
                                <a href='showbarcode.php?id=$bookid' class='btn btn-info btn-sm'>Show Barcode</a>
                            ";
                        }else if($row['parkingstatus']==2){
                            $seat = "Parking Over
                                <a href='showbarcode.php?id=$bookid' class='btn btn-success btn-sm'>Show Barcode</a>
                            ";
                        }
                        
                    }else{
                        $seat = "Not Confirmed<br/>
                        <a href='payment.php?booking_id=$bookid&amount=$amount' class='btn btn-success btn-sm'>Pay Now</a>
                        <a href='delete.php?booking_id=$bookid' class='btn btn-danger btn-sm'>Cancel</a>
                        ";
                    }
                    echo "<td>$seat</td>";
                    echo "</tr>";

                }
            ?>
        </tbody>
    </table>

</div>
<?php include_once('footer.php'); ?>