<?php
    require_once("checksum.php");
    if(isset($_GET['amount']) && isset($_GET['booking_id'])){
        $amount = $_GET['amount'];
        $booking_id = $_GET['booking_id'];
    }else{
        echo "Incorrect Perameters !";
        exit();
    }
    $order_id = "order".date('Ymdhmi').rand();
    define("merchantMid", "slJPcx77079296491932");
    // Key in your staging and production MID available in your dashboard
    define("merchantKey", "_rCvfDfp1_V9dh%S");
    // Key in your staging and production merchant key available in your dashboard
    define("orderId", $order_id);
    define("channelId", "WEB");
    define("custId", "cust123");
    define("mobileNo", "9781333368");
    define("email", "admin@simransingh.in");
    define("txnAmount", $amount);
    define("website", "WEBSTAGING");
    // This is the staging value. Production value is available in your dashboard
    define("industryTypeId", "Retail");
    // This is the staging value. Production value is available in your dashboard
    define("callbackUrl", "http://localhost/parking/paymentstatus.php?booking_id=$booking_id");
    $paytmParams = array();
    $paytmParams["MID"] = merchantMid;
    $paytmParams["ORDER_ID"] = orderId;
    $paytmParams["CUST_ID"] = custId;
    $paytmParams["MOBILE_NO"] = mobileNo;
    $paytmParams["EMAIL"] = email;
    $paytmParams["CHANNEL_ID"] = channelId;
    $paytmParams["TXN_AMOUNT"] = txnAmount;
    $paytmParams["WEBSITE"] = website;
    $paytmParams["INDUSTRY_TYPE_ID"] = industryTypeId;
    $paytmParams["CALLBACK_URL"] = callbackUrl;
    $paytmChecksum = getChecksumFromArray($paytmParams, merchantKey);
    $transactionURL = "https://securegw-stage.paytm.in/theia/processTransaction";
    // $transactionURL = "https://securegw.paytm.in/theia/processTransaction"; // for production
?>
<html>
    <head>
        <title>Merchant Checkout Page</title>
    </head>
    <body>
        <center><h1>Please do not refresh this page...</h1></center>
        <form method='post' action='<?php echo $transactionURL; ?>' name='f1'>
            <?php
                foreach($paytmParams as $name => $value) {
                    echo '<input type="hidden" name="' . $name .'" value="' . $value . '">';
                }
            ?>
            <input type="hidden" name="CHECKSUMHASH" value="<?php echo $paytmChecksum ?>">
        </form>
        <script type="text/javascript">
            document.f1.submit();
        </script>
    </body>
</html>