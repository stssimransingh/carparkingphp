<?php include_once('header.php'); ?>
<div class="col-md-12 bg-light p-5">
    <h2>Add Parking</h2>
    <div class="row">
        <div class="col-md-6 mt-5">
            <?php
                if(isset($_POST['parking_name'])){
                    $parking_name = $_POST['parking_name'];
                    $parking_address = $_POST['parking_address'];
                    $parking_space = $_POST['parking_space'];
                    $parking_price = $_POST['parking_price'];
                    $qry = "INSERT INTO parkings (parking_name, parking_address, parking_space, parking_price)
                    VALUES ('$parking_name','$parking_address','$parking_space','$parking_price')";
                    if($connect->query($qry)){
                        echo "<div class='alert alert-success'>Parking Added Successfully</div>";
                    }else{
                        echo "<div class='alert alert-danger'>There is an error while adding your parking</div>";
                    }
                }
            ?>
            <form action="" method="post">
                <label for="">Parking Name</label>
                <input type="text" name="parking_name" class="form-control" required/>
                <label for="">Parking Address</label>
                <input type="text" name="parking_address" class="form-control" required/>
                <label for="">Total Space</label>
                <input type="number" name="parking_space" class="form-control" required/>
                <label for="">Price per hour</label>
                <input type="number" name="parking_price" class="form-control"  required/>
                <input type="submit" class="btn btn-success mt-3"/>
            </form>
        </div>
    </div>
</div>
<?php include_once('footer.php'); ?>