<?php include_once('header.php'); ?>
<div class="col-md-12 bg-light p-5">
    <h1>Callback Request</h1>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Sr.No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Request</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $qry = "SELECT * FROM contacts";
            $res = $connect->query($qry);
            $i = 1;
            while($row = $res->fetch_assoc()){
                echo "<tr>";
                echo "<td>$i</td>";
                echo "<td>".$row['name']."</td>";
                echo "<td>".$row['email']."</td>";
                echo "<td>".$row['phone']."</td>";
                echo "<td>".$row['status']."</td>";
                echo "</tr>";
                $i++;
            }
            ?>
        </tbody>
    </table>
</div>
<?php include_once('footer.php'); ?>